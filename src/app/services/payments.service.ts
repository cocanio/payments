import { Injectable } from '@angular/core';
import { Payment } from '../models/payment';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  //Subject
  private readonly _payments = new BehaviorSubject<Payment[]>([]);

  //Observable array
  readonly payments$ = this._payments.asObservable();

  //Total
  total = 0;

  constructor() { }

  //Get total amount
  public calcTotal(){
    //Variable
    let total: number = 0;
    //Sum all
    this.payments.forEach((payment: Payment) => {
      total += payment.amount;
    });
    //Result
    this.total = total;
  }

  //Read all payments
  get payments(): Payment[] {
    return this._payments.getValue();
  }

  //Set payments
  set payments(arr: Payment[]){
    this._payments.next(arr);
    this.calcTotal();
  }

  //Add payment
  public add(payment: Payment){
    this.payments = [
      ...this.payments,
      payment
    ];
    this.calcTotal();
  }
  
  //Update payment
  public update(paymentUpdate: Payment){
    this.payments = this.payments.map(
      (payment: Payment) => payment.id == paymentUpdate.id?paymentUpdate:payment
    );
    this.calcTotal();
  }

  //Remove payment
  public remove(id: string){
    this.payments = this.payments.filter(
      (payment: Payment) => payment.id !== id
    );
    this.calcTotal();
  }

}
