import * as uuid from 'uuid'
/**
 * Payment model
 */
export class Payment {
    //Id: Random number
    private _id: string;
    //Date of the payment: No more than 7 days old to register
    private _date: Date;
    //Amount of the payment: Format 2 decimal points
    private _amount: number;
    //Description of the payment: Limit 255 chars
    private _description: string;

    /************* Contructor *********/

    constructor(date: Date, amount: number, description: string) {
        this._id = uuid.v4();
        this.date = date;
        this.amount = amount;
        this.description = description;
    }
    
    /************** Id **************/

    //Get date
    get id(): string{
        return this._id;
    }

    /************** Date **************/

    //Set date
    set date(date: Date){
        this._date = date;
    }

    //Get date
    get date(): Date{
        return this._date;
    }

    /************** Amount **************/

    //Set amount
    set amount(amount: number) {
        this._amount = amount;
    }

    //Get amount
    get amount(): number {
        return this._amount;
    }

    /************** Description **************/

    //Set description
    set description(description: string) {
        //If more than 255 characters
        if (description.length > 255) {
            description = description.substring(0, 255);
        }
        this._description = description.trim();
    }

    //Get description
    get description(): string {
        return this._description;
    }

}