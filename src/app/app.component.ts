import { Component, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { PaymentsService } from './services/payments.service';
import { Payment } from './models/payment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

  //Payment service
  paymentsService: PaymentsService = new PaymentsService();

  //Current payment for form
  currentPayment: Payment;

  //View state
  type: string = 'List';

  constructor() {
    
  }

  //Edit payment
  edit(payment: Payment) {
    //Current payment for edit
    this.currentPayment = payment;
    //Edit state
    this.type = 'Edit';
  }

  //Create new payment
  create() {
    //Current payment for creation
    this.currentPayment = new Payment(new Date(), 1, "");
    //Creation state
    this.type = 'Create';
  }

  //Form save
  save() {
    //Create 
    if (this.type == 'Create') {
      //Add
      this.paymentsService.add(this.currentPayment);
    } else {
      //Update
      this.paymentsService.update(this.currentPayment);
    }
    //Reset
    this.reset();
  }

  //Delete
  delete(payment: Payment) {
    this.paymentsService.remove(payment.id);
    //Reset view
    this.reset();
  }

  //Cancel form
  cancel() {
    //Reset view
    this.reset();
  }

  //Reset view to list
  reset() {
    //Set null 
    this.currentPayment = null;
    //List state
    this.type = 'List';
  }

}
