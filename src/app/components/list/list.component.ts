import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Payment } from '../../models/payment';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  //Payments array
  @Input() payments: Payment[];

  //create emitter
  @Output() create: EventEmitter<any> = new EventEmitter<any>();

  //Create emitter
  @Output() edit: EventEmitter<Payment> = new EventEmitter<Payment>();

  //Create emitter
  @Output() delete: EventEmitter<Payment> = new EventEmitter<Payment>();

  //Payment to delete
  deletePayment: Payment;

  //Callback create
  callCreate() {
    this.create.emit();
  }

  //Callback edit
  callEdit(payment: Payment) {
    this.edit.emit(payment);
  }

  //Confirm delete
  confirmDelete(content, payment: Payment) {
    //Payment to delete
    this.deletePayment = payment;
    //Open modal
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      //Confirm
      if (result == 'confirm') {
        this.delete.emit(payment);
      }
    }, (reason) => {});
  }

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

}
