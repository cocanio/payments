import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Payment } from '../../models/payment';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {

  //Type of action
  @Input() type: string;

  //Payment model
  @Input() payment: Payment;

  //Cancel emitter
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

  //Save emitter
  @Output() save: EventEmitter<Payment> = new EventEmitter<Payment>();

  //Form group
  paymentForm: FormGroup;

  //Validation dates
  minDate: string;
  maxDate: string;

  //callCancel
  callCancel() {
    this.cancel.emit();
  }

  //Call save
  callSave(status: string) {
    if(status == 'VALID'){
      //Date
      this.payment.date = this.paymentForm.value.date;
      //Amount to float
      this.payment.amount = parseFloat(this.paymentForm.value.amount);
      //Description
      this.payment.description = this.paymentForm.value.description;
      //Emit payment
      this.save.emit(this.payment);
    }
  }

  constructor(){
  }

  //
  ngOnInit(): void {
    //Past 7 days
    this.minDate = moment().add(-7, 'days').format('YYYY-MM-DD');
    //Today
    this.maxDate = moment().format('YYYY-MM-DD');
    //Form group
    this.paymentForm = new FormGroup({
      //Date
      date: new FormControl(moment(this.payment.date).format('YYYY-MM-DD'), [Validators.required]),
      //Amount
      amount: new FormControl(this.payment.amount.toFixed(2), [Validators.required, Validators.pattern(/^[0-9]+\.[0-9]{2}$/)]),
      //Description
      description: new FormControl(this.payment.description, Validators.maxLength(255))
    });
  }

}
