import { Component, OnInit, Input } from '@angular/core';
import { PaymentsService } from '../../services/payments.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  //payments value
  @Input() total: number;

  constructor() { }

  ngOnInit(): void {
  }

}
